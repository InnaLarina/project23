﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

namespace HomeWork
{
    class DesignMotivator
    {
        public PictureBox PBox { get; set; }
        public string PathFile { get; set; }
        public Size PBoxSize { get; set; }
        public string Title { get; set; }
        public DesignMotivator()
        {
            PictureBox pBox = new PictureBox();
        }
        public DesignMotivator(string pathFile, int width, int height, string title)
        {
            PBox = new PictureBox();
            PBoxSize = new Size(width, height);
            Title = title;
            PathFile = pathFile;
        }

        public void CreateMotivator()
        {
            ControlImage ci = new ControlImage(PBox);
            ci.CreateImage(PathFile);

            SizeImage si = new SizeImage(PBox);
            si.ResizeImage(PBoxSize);

            TextOnImage ti = new TextOnImage(PBox);
            ti.WriteTextOnImage(Title);

            FrameImage fi = new FrameImage(PBox);
            fi.FrameOnImage();

        }
        public void SetTitle(string title)
        {
            this.Title = title;
            CreateMotivator();
        }
        public void ResizeMotivator(int width, int height)
        {
            SizeImage si = new SizeImage(PBox);
            si.ResizeImage(new Size(width, height));
        }
        public void SaveMotivator(string pathDestination)
        {
            PictureToFile pf = new PictureToFile(PBox);
            pf.PictureIntoFile(pathDestination);
        }
    }
}
