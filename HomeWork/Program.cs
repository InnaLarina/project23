﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Threading.Tasks;

namespace HomeWork
{
    class Program
    {
        static void Main(string[] args)
        {
            string pathName = @"D:\C#Education\Git\Fasade\HomeWork\HomeWork\Materials\balet.jpg";
            string resultNameT = @"D:\C#Education\Git\Fasade\HomeWork\HomeWork\Result\motivator.bmp";
            string resultName = "";
            DesignMotivator dm = new DesignMotivator(pathName, 300, 150, "Все получится!");
            try
            {
                dm.CreateMotivator();
                resultName = resultNameT.Replace(".", "1.");
                dm.SaveMotivator(resultName);
                dm.ResizeMotivator(400, 270);
                resultName = resultNameT.Replace(".", "2.");
                dm.SaveMotivator(resultName);
                dm.SetTitle("Учись!");
                resultName = resultNameT.Replace(".", "3.");
                dm.SaveMotivator(resultName);
            }
            catch (FileNotFoundException e)
            {
                Console.WriteLine(e.Message);
            }
            catch (ArgumentException e)
            {
                Console.WriteLine(pathName + ". Не найден файл!");
            }
            Console.ReadKey();
        }
    }
}
