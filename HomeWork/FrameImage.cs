﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

namespace HomeWork
{
    class FrameImage
    {
        public PictureBox PBox { get; }
        public FrameImage(PictureBox pbox)
        {
            this.PBox = pbox;
        }
        public void FrameOnImage()
        {
            if (PBox.Image != null)
            {
                Bitmap bmp = new Bitmap(PBox.Image, new Size(PBox.Width, PBox.Height));
                Pen pen = new Pen(Color.Plum);
                pen.Width = 5;
                using (Graphics g = Graphics.FromImage(bmp))
                {
                    using (var font = new Font("Arial", 14))
                    {
                        g.DrawRectangle(pen, pen.Width / 2, pen.Width / 2, PBox.ClientSize.Width - pen.Width, PBox.ClientSize.Height - pen.Width);
                    }
                }
                PBox.Image = (Image)bmp;
            }
        }
    }
}
