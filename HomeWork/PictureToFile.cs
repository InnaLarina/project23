﻿using System.IO;
using System.Windows.Forms;
using System.Drawing;

namespace HomeWork
{
    class PictureToFile
    {
        public PictureBox PBox { get; }
        public PictureToFile(PictureBox pbox)
        {
            this.PBox = pbox;
        }
        public void PictureIntoFile(string filepath)
        {
            try
            {
                Bitmap bmap = PBox.Image as Bitmap;
                if (File.Exists(filepath))
                    File.Delete(filepath);
                bmap.Save(filepath, System.Drawing.Imaging.ImageFormat.Bmp);

            }
            catch (System.Runtime.InteropServices.ExternalException e)
            {
                throw new FileNotFoundException("File " + filepath + " doesn't exist.", e);
            }
        }
    }
}
