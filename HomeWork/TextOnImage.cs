﻿using System;
using System.Windows.Forms;
using System.Drawing;
using System.Text;
using System.Threading.Tasks;

namespace HomeWork
{
    class TextOnImage
    {
        public PictureBox PBox { get; }
        public TextOnImage(PictureBox pbox)
        {
            this.PBox = pbox;
        }
        public void WriteTextOnImage(string messageToSoul)
        {
            if (PBox.Image != null)
            {
                Bitmap bmp = new Bitmap(PBox.Image);
                using (Graphics g = Graphics.FromImage(bmp))
                {
                    using (var font = new Font("Arial", 14))
                    {
                        g.DrawString(messageToSoul, font, Brushes.Gold, new Point(20, 20));
                    }
                }
                PBox.Image = (Image)bmp;
            }
        }
    }
}
