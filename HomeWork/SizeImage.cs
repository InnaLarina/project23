﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

namespace HomeWork
{
    class SizeImage
    {
        public PictureBox PBox { get; }
        public SizeImage(PictureBox pbox)
        {
            this.PBox = pbox;
        }
        public void ResizeImage(Size pbSize)
        {
            PBox.ClientSize = pbSize;
            if (PBox.Image != null)
            {
                Bitmap bmp = new Bitmap(PBox.Image, pbSize);
                PBox.Image = (Image)bmp;
            }
        }
    }
}
