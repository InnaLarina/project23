﻿using System;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;

namespace HomeWork
{
    class ControlImage
    {
        public PictureBox PBox { get; }
        public ControlImage(PictureBox pbox)
        {
            this.PBox = pbox;
        }
        public void CreateImage(string pathName)
        {
            try
            {
                Bitmap bmap;
                bmap = new Bitmap(pathName);
                PBox.SizeMode = PictureBoxSizeMode.Zoom;
                PBox.Image = (Image)bmap;
            }
            catch (ArgumentException e)
            {
                throw new ArgumentException(e.Message, e);
            }

        }
    }
}
