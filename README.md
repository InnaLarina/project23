Разработано Консольное приложение, реализуюшее шаблон Фасад

Клиент обращается к классу DesignMotivator с методами CreateMotivator(),SetTitle(string title),ResizeMotivator(int width, int height),SaveMotivator(string pathDestination)
Методы используют классы
ControlImage: поле типа PictureBox, конструктор, метод CreateImage(string pathName), рисующий картинку на поле PictureBox
SizeImage: поле типа PictureBox, конструктор, метод ResizeImage(int xSize, int ySize), изменяющий размер картинки.
FrameImage: поле типа PictureBox, конструктор, метод FrameOnImage(), рисующий рамочку.
TextOnImage: поле типа PictureBox, конструктор, метод WriteTextOnImage(string messageToSoul), делающий надпись.

Картинки для тестирования программы находятся по пути \Materials

Файлы с сохраненным результатом в папке \Result 

Код тестирования в классе Program